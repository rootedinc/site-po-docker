FROM ubuntu:latest
ADD start_wsgi.sh /usr/bin/
ADD update_site.sh /usr/bin/
COPY nginx_conf.tar.gz /

RUN apt-get update && apt-get install -y python3 python3-pip git nginx && pip3 install virtualenv

RUN /bin/bash -c "virtualenv -p /usr/bin/python3.5 /uwsgi \
	&& cd /uwsgi \
	&& source bin/activate \
	&& pip install uwsgi Django\
	&& git clone https://bitbucket.org/rootedinc/site-po \
	&& cd site-po \
	&& ./init_django.sh \
	&& ./init_production.sh"

RUN /bin/bash -c "rm -rf /etc/nginx/* \
	&& mv /nginx_conf.tar.gz /etc/nginx/ \
	&& cd /etc/nginx/ \
	&& tar xvf nginx_conf.tar.gz \
	&& rm -rf nginx_conf.tar.gz"

RUN /bin/bash -c "chown root:root -R /etc/nginx \
	&& chown root:root -R /uwsgi \
	&& chmod 755 -R /uwsgi"

CMD /bin/bash -c "service nginx start && /usr/bin/start_wsgi.sh"
