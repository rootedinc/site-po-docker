#!/bin/bash

cd /uwsgi
source bin/activate
cd site-po
git fetch origin
git reset --hard origin/master
./init_production.sh
