#!/bin/bash

cd /uwsgi
source bin/activate
cd site-po
uwsgi --socket /sitepo.sock --chown-socket=www-data --chmod-socket=600 --module sitepo.wsgi
